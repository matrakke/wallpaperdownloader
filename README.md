# WallpaperDownloader

Everyone asks me for backgrounds when I show my desktop screenshots.

Well, I get my wallpapers from https://wallpaperscraft.com/

But searching through all the wallpapers is time consuming. To resolve that problem, I wrote a
small program in Python with the PyQt5 interface.

The Windowsusers just can download the setup file to install it.

On Linux run **git clone https://gitlab.com/matrakke/wallpaperdownloader.git**

cd wallpaperdownloader
run python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

Run the main.py file.

Enjoy


